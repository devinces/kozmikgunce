---
layout: page
title: Kozmik Günce Hakkında
#subtitle: Why you'd want to go on a date with me
---
![Singing Boy]({{ site.url }}/assets/vault-boy-swap.jpg)

Boğaziçi Üniversitesi ve İTÜ dersliklerinde tanışmış birkaç genç fizikçiyiz. Aklımıza takılanları ya da bir zamanlar takılıp sonra çözülenleri fırsat buldukça paylaşmak istiyoruz. Öncelikli alanlar astrofizik, matematiksel fizik ve kozmoloji.

Yazılar, içerikleri çok çeşitli olmakla beraber, bir ortak noktaya sahip olacak: Bilim. Hakemli dergiler gibi değil elbette. Popüler bilim haberleri yapan sitelere de benzemeyecektir. Burada, elimizden geldiğince, ilgilendiğimiz ve başkalarının da ilgilenebileceği veya başımıza gelen ve başkalarının da başına gelebilecek olaylara, konulara değineceğiz.

İyi seyirler.
