---
layout: page
title: Kaynaklar
#subtitle: Why you'd want to go on a date with me
---
## Bağlantılar

- [Fizik Haftası](http://fizikhaftasi.itu.edu.tr/)

## Güzel Bloglar

- [Edge](http://edge.org/)
- [Gök Günce](http://www.gokgunce.net/)
- [Non-elephant Dynamics](http://nonelephantdynamics.blogspot.com.tr/)
- [Preposterous Universe](http://www.preposterousuniverse.com/blog/)

## Vidyo Dersler

- [Fiziksel Matematik](https://www.youtube.com/playlist?list=PLpyIij4nP6QKeode7spPag-4JPjTC2U8g)
- [Kuantum Mekaniği](https://www.youtube.com/playlist?list=PL0F530F3BAF8C6FCC)
- [Matematiksel Fizik](https://www.youtube.com/playlist?list=PLiUVvsKxTUr66oLF6Pzirc1EgSstMbRZR)
- [İstatistik Fizik](https://www.youtube.com/playlist?list=PLCC901B3A9445042D)

## Web Kitapları

- [The Algebra of GUT's](http://math.ucr.edu/~huerta/guts/)
